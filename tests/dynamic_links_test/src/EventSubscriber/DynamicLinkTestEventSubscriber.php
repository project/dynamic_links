<?php

declare(strict_types=1);

namespace Drupal\dynamic_links_test\EventSubscriber;

use Drupal\dynamic_links\Event\DynamicLinkRedirectsEvent;
use Drupal\dynamic_links\Event\DynamicLinkRoutesEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns test responses.
 */
class DynamicLinkTestEventSubscriber implements EventSubscriberInterface {

  /**
   * Constructs the object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(protected readonly RequestStack $requestStack) {}

  /**
   * Handles the dynamic link redirects alter event.
   */
  public function redirectsAlter(DynamicLinkRedirectsEvent $event): void {
    if ($this->requestStack->getCurrentRequest()->query->get('prefer_last') === '1') {
      $redirects = &$event->getRedirects();
      $redirects = array_reverse($redirects);
    }
  }

  /**
   * Handles the dynamic link routes alter event.
   */
  public function routesAlter(DynamicLinkRoutesEvent $event): void {
    if ($this->requestStack->getCurrentRequest()->query->get('prefer_last') === '1') {
      $routes = &$event->getRoutes();
      $routes = array_reverse($routes);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      DynamicLinkRedirectsEvent::buildEventName() => 'redirectsAlter',
      DynamicLinkRoutesEvent::buildEventName() => 'routesAlter',
    ];
  }

}
