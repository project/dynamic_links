<?php

declare(strict_types=1);

namespace Drupal\dynamic_links_test\Controller;

use Symfony\Component\HttpFoundation\Request;

/**
 * Returns test responses.
 */
class DynamicLinkTestController {

  /**
   * Builds the test response.
   */
  public function build(Request $request): array {
    return [
      '#type' => 'container',
      '#attributes' => [
        'data-path-info' => $request->getPathInfo(),
      ],
    ];
  }

}
