<?php

declare(strict_types=1);

namespace Drupal\Tests\dynamic_links\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\dynamic_links\DynamicLinkInterface;

/**
 * @covers \Drupal\dynamic_links\Controller\DynamicLinkController
 * @covers \Drupal\dynamic_links\Form\DynamicLinkForm
 */
class DynamicLinkTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dynamic_links_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests configuration combinations.
   */
  public function test(): void {
    // Define the test configurations.
    $configs['routes_no.subrequest_no'] = [
      'status' => TRUE,
      'id' => 'front',
      'path' => '/dynamic-front',
      'use_subrequest' => FALSE,
      'as_routes' => FALSE,
      'redirects' => [
        '/dynamic-links-test/route-1',
        '/dynamic-links-test/route-2',
        '/dynamic-links-test/route-3',
      ],
      'expectations' => [
        [
          'permissions' => ['dynamic_links_test route_1'],
          'expected' => '/dynamic-links-test/route-1',
          'path_info' => '/dynamic-links-test/route-1',
          'code' => 200,
        ],
        [
          'permissions' => ['dynamic_links_test route_2'],
          'expected' => '/dynamic-links-test/route-2',
          'path_info' => '/dynamic-links-test/route-2',
          'code' => 200,
        ],
        [
          'permissions' => ['dynamic_links_test route_3'],
          'expected' => '/dynamic-links-test/route-3',
          'path_info' => '/dynamic-links-test/route-3',
          'code' => 200,
        ],
        [
          'query' => ['prefer_last' => '1'],
          'permissions' => [
            'dynamic_links_test route_1',
            'dynamic_links_test route_2',
            'dynamic_links_test route_3',
          ],
          'expected' => '/dynamic-links-test/route-3',
          'path_info' => '/dynamic-links-test/route-3',
          'code' => 200,
        ],
        [
          'permissions' => [],
          'expected' => '/dynamic-front',
          'path_info' => FALSE,
          'code' => 403,
        ],
      ],
    ];

    $configs['routes_yes.subrequest_no'] = [
      'id' => "{$configs['routes_no.subrequest_no']['id']}_as_routes",
      'path' => "{$configs['routes_no.subrequest_no']['path']}/as-routes",
      'as_routes' => TRUE,
      'expectations' => [
        [
          'permissions' => ['dynamic_links_test route_1'],
          'expected' => '/dynamic-links-test/route-1',
          'path_info' => '/dynamic-links-test/route-1',
          'code' => 200,
        ],
        [
          'permissions' => ['dynamic_links_test route_2'],
          'expected' => '/dynamic-links-test/route-2',
          'path_info' => '/dynamic-links-test/route-2',
          'code' => 200,
        ],
        [
          'permissions' => ['dynamic_links_test route_3'],
          'expected' => '/dynamic-links-test/route-3',
          'path_info' => '/dynamic-links-test/route-3',
          'code' => 200,
        ],
        [
          'query' => ['prefer_last' => '1'],
          'permissions' => [
            'dynamic_links_test route_1',
            'dynamic_links_test route_2',
            'dynamic_links_test route_3',
          ],
          'expected' => '/dynamic-links-test/route-3',
          'path_info' => '/dynamic-links-test/route-3',
          'code' => 200,
        ],
        [
          'permissions' => [],
          'expected' => '/dynamic-front/as-routes',
          'path_info' => FALSE,
          'code' => 403,
        ],
      ],
    ] + $configs['routes_no.subrequest_no'];

    $configs['routes_no.subrequest_yes'] = [
      'id' => "{$configs['routes_no.subrequest_no']['id']}_subrequest",
      'path' => "{$configs['routes_no.subrequest_no']['path']}/subrequest",
      'use_subrequest' => TRUE,
      'expectations' => [
        [
          'permissions' => ['dynamic_links_test route_1'],
          'expected' => '/dynamic-front/subrequest',
          'path_info' => '/dynamic-links-test/route-1',
          'code' => 200,
        ],
        [
          'permissions' => ['dynamic_links_test route_2'],
          'expected' => '/dynamic-front/subrequest',
          'path_info' => '/dynamic-links-test/route-2',
          'code' => 200,
        ],
        [
          'permissions' => ['dynamic_links_test route_3'],
          'expected' => '/dynamic-front/subrequest',
          'path_info' => '/dynamic-links-test/route-3',
          'code' => 200,
        ],
        [
          'permissions' => [],
          'expected' => '/dynamic-front/subrequest',
          'path_info' => FALSE,
          'code' => 403,
        ],
      ],
    ] + $configs['routes_no.subrequest_no'];

    $configs['routes_yes.subrequest_yes'] = [
      'id' => "{$configs['routes_yes.subrequest_no']['id']}_subrequest",
      'path' => "{$configs['routes_yes.subrequest_no']['path']}/subrequest",
      'use_subrequest' => TRUE,
      'expectations' => [
        [
          'permissions' => ['dynamic_links_test route_1'],
          'expected' => '/dynamic-front/as-routes/subrequest',
          'path_info' => '/dynamic-links-test/route-1',
          'code' => 200,
        ],
        [
          'permissions' => ['dynamic_links_test route_2'],
          'expected' => '/dynamic-front/as-routes/subrequest',
          'path_info' => '/dynamic-links-test/route-2',
          'code' => 200,
        ],
        [
          'permissions' => ['dynamic_links_test route_3'],
          'expected' => '/dynamic-front/as-routes/subrequest',
          'path_info' => '/dynamic-links-test/route-3',
          'code' => 200,
        ],
        [
          'permissions' => [],
          'expected' => '/dynamic-front/as-routes/subrequest',
          'path_info' => FALSE,
          'code' => 403,
        ],
      ],
    ] + $configs['routes_yes.subrequest_no'];

    $admin = $this->drupalCreateUser(['administer dynamic_link']);
    $add_form_url = Url::fromRoute('entity.dynamic_link.add_form');
    $storage = $this->container->get('entity_type.manager')->getStorage('dynamic_link');
    $route_collection = $this->container->get('router')->getRouteCollection();
    foreach ($configs as $key => $config) {
      // Create a dynamic link.
      $this->drupalLogin($admin);
      $this->drupalGet($add_form_url);
      $edit = $config;
      $edit['status'] = $edit['status'] ? '1' : '0';
      $edit['raw_redirects'] = implode("\n", $edit['redirects']);
      $edit['as_routes'] = $edit['as_routes'] ? '1' : '0';
      $edit['use_subrequest'] = $edit['use_subrequest'] ? '1' : '0';
      unset($edit['expectations'], $edit['redirects']);
      $this->submitForm($edit, 'Save');

      // Check if the link has been created.
      $link = $storage->loadUnchanged($config['id']);
      $this->assertInstanceOf(DynamicLinkInterface::class, $link, "$key: dynamic link not created");

      // Check if the link is editable.
      $this->drupalGet($link->toUrl('edit-form'));
      $this->submitForm([], 'Save');
      $this->assertSession()->statusCodeEquals(200);

      // Check if editing breaks the link.
      $link = $storage->loadUnchanged($config['id']);
      $this->assertInstanceOf(DynamicLinkInterface::class, $link, "$key: dynamic link broken after editing.");

      // Check the properties of the link.
      $this->assertEquals($link->status(), $config['status'], "$key: 'status' config incorrect");
      $this->assertEquals($link->getPath(), $config['path'], "$key: 'path' config incorrect");
      $this->assertEquals($link->useSubrequest(), $config['use_subrequest'], "$key: 'use_subrequest' config incorrect");

      if ($config['as_routes']) {
        $this->assertNull($link->getRedirects(), "$key: if 'as_routes' is TRUE, 'redirects' must be NULL");
        $routes = [];
        foreach ($config['redirects'] as $redirect) {
          $redirect_url = Url::fromUserInput($redirect);
          $route_name = $redirect_url->getRouteName();
          $route_params = $redirect_url->getRouteParameters();
          foreach (array_keys($route_collection->get($route_name)->getDefaults()) as $default_param) {
            unset($route_params[$default_param]);
          }
          $routes[] = [
            'name' => $route_name,
            'parameters' => $route_params,
            'options' => $redirect_url->getOptions(),
          ];
        }
        $this->assertEquals($link->getRoutes(), $routes, "$key: 'routes' config incorrect");
      }
      else {
        $this->assertNull($link->getRoutes(), "$key: if 'as_routes' is FALSE, 'routes' must be NULL");
        $this->assertEquals($link->getRedirects(), $config['redirects'], "$key: 'redirects' config incorrect");
      }
      $this->drupalLogout();

      // Check the link redirect results.
      foreach ($config['expectations'] as $variant) {
        $this->drupalLogin($this->drupalCreateUser($variant['permissions']));
        $this->drupalGet($link->getDynamicUrl()->setOption('query', $variant['query'] ?? []));
        $session = $this->assertSession();
        $session->statusCodeEquals($variant['code']);
        $session->addressEquals(Url::fromUserInput($variant['expected'])->setAbsolute()->toString());
        if ($variant['path_info']) {
          $elements = $this->xpath("//div[@data-path-info=\"{$variant['path_info']}\"]");
          $this->assertCount(1, $elements, "$key: 'path_info' element not found");
        }
        $this->drupalLogout();
      }
    }
  }

}
