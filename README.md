# Dynamic links

This module provides the ability to create links that combine several other
links and redirect the user to the first available link.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

## Configuration

1. Go to the link creation form `/admin/structure/dynamic-link/add`.
2. Fill out the form and save.
