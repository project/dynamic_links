<?php

declare(strict_types=1);

namespace Drupal\dynamic_links\Routing;

use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\dynamic_links\Controller\DynamicLinkController;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for dynamic links.
 */
class DynamicLinkRoutes implements ContainerInjectionInterface {

  use AutowireTrait;

  /**
   * The route prefix.
   */
  protected const ROUTE_PREFIX = 'dynamic_link.';

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * Returns an array of route objects for dynamic links.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes(): array {
    /** @var \Drupal\dynamic_links\DynamicLinkInterface[] $links */
    $links = $this->entityTypeManager->getStorage('dynamic_link')->loadMultiple();
    $routes = [];
    $controller = DynamicLinkController::class;
    $route_template = new Route(
      '',
      [
        '_controller' => "$controller::redirect",
        'dynamic_link' => '',
      ],
      [
        '_custom_access' => "$controller::access",
      ],
      [
        'parameters' => [
          'dynamic_link' => [
            'type' => 'entity:dynamic_link',
          ],
        ],
      ]
    );
    foreach ($links as $link_id => $link) {
      if ($link->status()) {
        $routes[static::getRouteName($link_id)] = (clone $route_template)
          ->setPath($link->getPath())
          ->setDefault('dynamic_link', $link_id);
      }
    }
    return $routes;
  }

  /**
   * Returns the route name for a dynamic link.
   *
   * @param string $link_id
   *   The link ID.
   *
   * @return string
   *   The route name.
   */
  public static function getRouteName(string $link_id): string {
    return static::ROUTE_PREFIX . $link_id;
  }

}
