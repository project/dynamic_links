<?php

declare(strict_types=1);

namespace Drupal\dynamic_links;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;

/**
 * Provides an interface defining a dynamic link entity type.
 */
interface DynamicLinkInterface extends ConfigEntityInterface {

  /**
   * Returns the URL.
   *
   * @return string
   *   The URL.
   */
  public function getPath(): string;

  /**
   * Determines whether a subrequest should be used instead of a redirect.
   *
   * @return bool
   *   TRUE if a subrequest should be used, FALSE otherwise.
   */
  public function useSubrequest(): bool;

  /**
   * Returns an array of redirects.
   *
   * @return string[]|null
   *   An array of redirects, or NULL if using routes.
   */
  public function getRedirects(): ?array;

  /**
   * Returns an array of routes.
   *
   * @return array<int, array{name: string, parameters: array<string, string>, options: array}>|null
   *   An array of routes, or NULL if using redirects.
   */
  public function getRoutes(): ?array;

  /**
   * Returns the first available URL.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check access for.
   *
   * @return array{url: \Drupal\Core\Url|null, cacheability: \Drupal\Core\Cache\CacheableMetadata}
   *   The array of the first available URL and its cacheability metadata.
   */
  public function getFirstAvailable(AccountInterface $account): array;

  /**
   * Returns the dynamic URL.
   *
   * @return \Drupal\Core\Url|null
   *   The dynamic URL, or NULL if not available.
   */
  public function getDynamicUrl(): ?Url;

}
