<?php

declare(strict_types=1);

namespace Drupal\dynamic_links\Entity;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\dynamic_links\DynamicLinkInterface;
use Drupal\dynamic_links\Event\DynamicLinkEventBase;
use Drupal\dynamic_links\Event\DynamicLinkRedirectsEvent;
use Drupal\dynamic_links\Event\DynamicLinkRoutesEvent;
use Drupal\dynamic_links\Routing\DynamicLinkRoutes;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Defines the dynamic link entity type.
 *
 * @ConfigEntityType(
 *   id = "dynamic_link",
 *   label = @Translation("Dynamic link"),
 *   label_collection = @Translation("Dynamic links"),
 *   label_singular = @Translation("dynamic link"),
 *   label_plural = @Translation("dynamic links"),
 *   label_count = @PluralTranslation(
 *     singular = "@count dynamic link",
 *     plural = "@count dynamic links",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\dynamic_links\DynamicLinkListBuilder",
 *     "route_provider" = {
 *        "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *      },
 *     "form" = {
 *       "add" = "Drupal\dynamic_links\Form\DynamicLinkForm",
 *       "edit" = "Drupal\dynamic_links\Form\DynamicLinkForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "link",
 *   admin_permission = "administer dynamic_link",
 *   links = {
 *     "collection" = "/admin/structure/dynamic-link",
 *     "add-form" = "/admin/structure/dynamic-link/add",
 *     "edit-form" = "/admin/structure/dynamic-link/{dynamic_link}",
 *     "delete-form" = "/admin/structure/dynamic-link/{dynamic_link}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *   },
 *   config_export = {
 *     "id",
 *     "path",
 *     "use_subrequest",
 *     "redirects",
 *     "routes",
 *   },
 * )
 */
class DynamicLink extends ConfigEntityBase implements DynamicLinkInterface {

  /**
   * The link ID.
   */
  protected string $id;

  /**
   * The link path.
   */
  protected string $path = '';

  /**
   * Whether to use a subrequest.
   */
  protected bool $use_subrequest = FALSE;

  /**
   * An array of redirect URLs.
   *
   * @var string[]|null
   */
  protected ?array $redirects = NULL;

  /**
   * An array of redirect routes.
   *
   * @var array<int, array{name: string, parameters: array<string, string>, options: array}>|null
   */
  protected ?array $routes = NULL;

  /**
   * {@inheritdoc}
   */
  public function useSubrequest(): bool {
    return $this->use_subrequest;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath(): string {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirects(): ?array {
    return $this->redirects;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoutes(): ?array {
    return $this->routes;
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstAvailable(AccountInterface $account): array {
    $available_url = NULL;
    $cacheability = new CacheableMetadata();
    if ($routes = $this->getRoutes()) {
      $event = new DynamicLinkRoutesEvent($this, $account, $cacheability, $routes);
      $this->dispatchEvent($event);
      foreach ($event->getRoutes() as $route) {
        $this->checkUrl(Url::fromRoute($route['name'], $route['parameters'], $route['options']), $account, $cacheability, $available_url);
      }
    }
    elseif ($redirects = $this->getRedirects()) {
      $event = new DynamicLinkRedirectsEvent($this, $account, $cacheability, $redirects);
      $this->dispatchEvent($event);
      foreach ($event->getRedirects() as $redirect) {
        $this->checkUrl(Url::fromUserInput($redirect), $account, $cacheability, $available_url);
      }
    }
    return [
      'url' => $available_url,
      'cacheability' => $cacheability,
    ];
  }

  /**
   * Checks if the given URL is accessible to the given user.
   */
  protected function checkUrl(Url $url, AccountInterface $account, CacheableMetadata $cacheability, ?Url &$available_url): void {
    $access = $url->access($account, TRUE);
    $cacheability->addCacheableDependency($access);
    if (!$available_url && $access->isAllowed()) {
      $available_url = $url;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicUrl(): ?Url {
    if (!$id = $this->id()) {
      return NULL;
    }
    return Url::fromRoute(DynamicLinkRoutes::getRouteName($id));
  }

  /**
   * Dispatches the event.
   */
  protected function dispatchEvent(DynamicLinkEventBase $event): void {
    foreach ([
      $event->buildEventName(),
      $event->buildEventName($event->getDynamicLink()->id()),
    ] as $event_name) {
      $this->getEventDispatcher()->dispatch($event, $event_name);
    }
  }

  /**
   * Returns the event dispatcher.
   *
   * @todo replace with DI after https://www.drupal.org/project/drupal/issues/2142515
   */
  protected function getEventDispatcher(): EventDispatcherInterface {
    return \Drupal::service('event_dispatcher');
  }

}
