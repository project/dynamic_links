<?php

declare(strict_types=1);

namespace Drupal\dynamic_links\Form;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Url;
use Drupal\dynamic_links\Entity\DynamicLink;
use Drupal\dynamic_links\Routing\DynamicLinkRoutes;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Dynamic link form.
 */
class DynamicLinkForm extends EntityForm {

  /**
   * The router builder.
   */
  protected RouteBuilderInterface $routerBuilder;

  /**
   * The router.
   */
  protected RouterInterface $router;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->routerBuilder = $container->get('router.builder');
    $instance->router = $container->get('router.no_access_checks');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\dynamic_links\DynamicLinkInterface $entity */
    $entity = $this->getEntity();
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $entity->status(),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Link ID'),
      '#default_value' => $entity->id(),
      '#required' => TRUE,
      '#placeholder' => 'front',
      '#machine_name' => [
        'exists' => [DynamicLink::class, 'load'],
      ],
    ];
    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link path'),
      '#default_value' => $entity->getPath(),
      '#required' => TRUE,
      '#placeholder' => 'tasks',
      '#field_prefix' => $this->getRequest()->getSchemeAndHttpHost() . '/',
      '#element_validate' => [[$this, 'validatePath']],
    ];

    $routes = $entity->getRoutes();
    $as_routes = $routes !== NULL;
    if ($as_routes) {
      $redirects = [];
      foreach ($routes as $route) {
        $redirects[] = Url::fromRoute($route['name'], $route['parameters'], $route['options'])->toString();
      }
    }
    else {
      $redirects = $entity->getRedirects();
    }
    $form['raw_redirects'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Redirect paths'),
      '#default_value' => $redirects ? implode("\n", $redirects) : '',
      '#description' => $this->t('Enter a list of paths. The user will be redirected to the first available one.'),
      '#required' => TRUE,
      '#placeholder' => "/tasks/all\n/tasks/own\n/user",
      '#element_validate' => [[$this, 'validateRawRedirects']],
    ];
    $form['use_subrequest'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use subrequest'),
      '#default_value' => $entity->useSubrequest(),
      '#description' => $this->t('Instead of redirecting, the content will be displayed at the specified link path.'),
    ];

    $form['routes'] = [
      '#type' => 'details',
      '#title' => $this->t('Storage mode'),
      '#open' => FALSE,
    ];
    $form['routes']['as_routes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Store redirect paths as routes'),
      '#default_value' => $as_routes,
      '#description' => $this->t("If you don't want to bind to paths, but to routes, use this option."),
    ];
    if ($as_routes) {
      $form['routes']['description'] = [
        '#type' => 'html_tag',
        '#tag' => 'pre',
        '#value' => Yaml::encode($routes),
      ];
    }

    $current_user = $this->currentUser();
    [
      'url' => $url,
      'cacheability' => $cacheability,
    ] = $entity->getFirstAvailable($current_user);
    $form['cacheability'] = [
      '#type' => 'details',
      '#title' => $this->t('Cacheability'),
      '#open' => FALSE,
    ];
    $form['cacheability']['current_user'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Current user: %name (@id)', [
        '%name' => $current_user->getAccountName(),
        '@id' => $current_user->id(),
      ]),
    ];
    $form['cacheability']['path'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Path: %path', ['%path' => $url?->toString() ?? '-']),
    ];
    $max_age = $cacheability->getCacheMaxAge();
    $max_age = $max_age === Cache::PERMANENT
      ? $this->t('Permanent')
      : $this->t('@age seconds', ['@age' => $max_age]);
    $cacheability = [
      'cache_contexts' => [
        'data' => $cacheability->getCacheContexts(),
        'label' => $this->t('Contexts'),
      ],
      'cache_tags' => [
        'data' => $cacheability->getCacheTags(),
        'label' => $this->t('Tags'),
      ],
      'cache_max_age' => [
        'data' => [$max_age],
        'label' => $this->t('Max age'),
      ],
    ];
    foreach ($cacheability as $key => $value) {
      if (!$value['data']) {
        continue;
      }
      $form['cacheability'][$key] = [
        '#type' => 'item',
        '#title' => $value['label'],
        'value' => [
          '#theme' => 'item_list',
          '#items' => $value['data'],
        ],
      ];
    }

    return $form;
  }

  /**
   * Form element validation handler for the 'path' element.
   */
  public function validatePath(array &$element, FormStateInterface $form_state): void {
    $path = '/' . trim($element['#value'], '/');
    $form_state->setValueForElement($element, $path);
    try {
      $url = Url::fromUserInput($path);
      $route_name = $url->isRouted() ? $url->getRouteName() : NULL;
      if ($route_name && $route_name !== DynamicLinkRoutes::getRouteName($form_state->getValue('id'))) {
        $form_state->setError($element, $this->t("The path '%path' is already routed by: %route", [
          '%path' => $path,
          '%route' => $route_name,
        ]));
      }
    }
    catch (\Throwable $e) {
      $form_state->setError($element, $this->t("The path '%path' is invalid: @message", [
        '%path' => $path,
        '@message' => $e->getMessage(),
      ]));
    }
  }

  /**
   * Form element validation handler for the 'redirects' element.
   */
  public function validateRawRedirects(array &$element, FormStateInterface $form_state): void {
    $redirects = [];
    foreach (explode("\n", trim($element['#value'])) as $redirect) {
      if ($redirect = trim($redirect)) {
        try {
          $url = Url::fromUserInput($redirect);
          if (!$url->isRouted()) {
            $form_state->setError($element, $this->t("The path '%path' is not a route: %route", [
              '%path' => $redirect,
              '%route' => $url->toString(),
            ]));
            continue;
          }
          $route_name = $url->getRouteName();
          if ($route_name === DynamicLinkRoutes::getRouteName($form_state->getValue('id'))) {
            $form_state->setError($element, $this->t('This dynamic link cannot redirect to itself: %path', [
              '%path' => $redirect,
            ]));
            continue;
          }
          $redirects[] = $redirect;
        }
        catch (\Throwable $e) {
          $form_state->setError($element, $this->t("The path '%path' is invalid: @message", [
            '%path' => $redirect,
            '@message' => $e->getMessage(),
          ]));
        }
      }
    }
    $form_state->setValueForElement($element, $redirects);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $redirects = $form_state->getValue('raw_redirects');
    if ($form_state->getValue('as_routes')) {
      $route_collection = $this->router->getRouteCollection();
      $routes = [];
      foreach ($redirects as $redirect) {
        $redirect_url = Url::fromUserInput($redirect);
        $route_name = $redirect_url->getRouteName();
        $route_params = $redirect_url->getRouteParameters();
        foreach (array_keys($route_collection->get($route_name)->getDefaults()) as $default_param) {
          unset($route_params[$default_param]);
        }
        $routes[] = [
          'name' => $route_name,
          'parameters' => $route_params,
          'options' => $redirect_url->getOptions(),
        ];
      }
      $form_state->setValue('routes', $routes);
      $form_state->setValue('redirects', NULL);
    }
    else {
      $form_state->setValue('routes', NULL);
      $form_state->setValue('redirects', $redirects);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    /** @var \Drupal\dynamic_links\DynamicLinkInterface $entity */
    $entity = $this->getEntity();
    $message_args = ['%label' => $entity->label()];
    $this->messenger()->addStatus(match ($result) {
      \SAVED_NEW => $this->t('Created new dynamic link %label.', $message_args),
      \SAVED_UPDATED => $this->t('Updated dynamic link %label.', $message_args),
    });
    $form_state->setRedirectUrl($entity->toUrl('collection'));
    $this->routerBuilder->rebuild();

    return $result;
  }

}
