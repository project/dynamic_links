<?php

declare(strict_types=1);

namespace Drupal\dynamic_links\Event;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\dynamic_links\DynamicLinkInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides base class for dynamic link events.
 */
abstract class DynamicLinkEventBase extends Event implements RefinableCacheableDependencyInterface {

  /**
   * Constructs the object.
   *
   * @param \Drupal\dynamic_links\DynamicLinkInterface $link
   *   The dynamic link.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheability
   *   The cacheable metadata.
   */
  public function __construct(
    protected readonly DynamicLinkInterface $link,
    protected readonly AccountInterface $account,
    protected readonly CacheableMetadata $cacheability,
  ) {}

  /**
   * Returns the account which triggered the event.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   The account.
   */
  public function getAccount(): AccountInterface {
    return $this->account;
  }

  /**
   * Returns the dynamic link which triggered the event.
   *
   * @return \Drupal\dynamic_links\DynamicLinkInterface
   *   The dynamic link.
   */
  public function getDynamicLink(): DynamicLinkInterface {
    return $this->link;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return $this->cacheability->getCacheContexts();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return $this->cacheability->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    return $this->cacheability->getCacheMaxAge();
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheContexts(array $cache_contexts): static {
    $this->cacheability->addCacheContexts($cache_contexts);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheTags(array $cache_tags): static {
    $this->cacheability->addCacheTags($cache_tags);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function mergeCacheMaxAge($max_age): static {
    $this->cacheability->mergeCacheMaxAge($max_age);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheableDependency($other_object): static {
    $this->cacheability->addCacheableDependency($other_object);
    return $this;
  }

  /**
   * Builds the event name.
   *
   * @param string|null $link_id
   *   (optional) The link ID to subscribe to the specific link's event.
   *
   * @return string
   *   The event name.
   */
  public static function buildEventName(?string $link_id = NULL): string {
    $event_name = static::class;
    if ($link_id !== NULL) {
      $event_name .= "::$link_id";
    }
    return $event_name;
  }

}
