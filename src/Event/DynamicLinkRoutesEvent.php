<?php

declare(strict_types=1);

namespace Drupal\dynamic_links\Event;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountInterface;
use Drupal\dynamic_links\DynamicLinkInterface;

/**
 * Provides a dynamic link routes event for event listeners.
 */
class DynamicLinkRoutesEvent extends DynamicLinkEventBase {

  /**
   * Constructs the object.
   *
   * @param \Drupal\dynamic_links\DynamicLinkInterface $link
   *   The dynamic link.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheability
   *   The cacheable metadata.
   * @param array<int, array{name: string, parameters: array<string, string>, options: array}> $routes
   *   The routes.
   */
  public function __construct(
    DynamicLinkInterface $link,
    AccountInterface $account,
    CacheableMetadata $cacheability,
    protected array $routes,
  ) {
    parent::__construct($link, $account, $cacheability);
  }

  /**
   * Returns an array of routes.
   *
   * @return array<int, array{name: string, parameters: array<string, string>, options: array}>
   *   An array of routes.
   */
  public function &getRoutes(): array {
    return $this->routes;
  }

  /**
   * Sets the routes.
   *
   * @param array<int, array{name: string, parameters: array<string, string>, options: array}> $routes
   *   An array of routes.
   */
  public function setRoutes(array $routes): void {
    $this->routes = $routes;
  }

}
