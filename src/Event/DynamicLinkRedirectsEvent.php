<?php

declare(strict_types=1);

namespace Drupal\dynamic_links\Event;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountInterface;
use Drupal\dynamic_links\DynamicLinkInterface;

/**
 * Provides a dynamic link redirects event for event listeners.
 */
class DynamicLinkRedirectsEvent extends DynamicLinkEventBase {

  /**
   * Constructs the object.
   *
   * @param \Drupal\dynamic_links\DynamicLinkInterface $link
   *   The dynamic link.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheability
   *   The cacheable metadata.
   * @param string[] $redirects
   *   The redirects.
   */
  public function __construct(
    DynamicLinkInterface $link,
    AccountInterface $account,
    CacheableMetadata $cacheability,
    protected array $redirects,
  ) {
    parent::__construct($link, $account, $cacheability);
  }

  /**
   * Returns an array of redirects.
   *
   * @return string[]
   *   An array of redirects.
   */
  public function &getRedirects(): array {
    return $this->redirects;
  }

  /**
   * Sets the redirects.
   *
   * @param string[] $routes
   *   An array of redirects.
   */
  public function setRedirects(array $routes): void {
    $this->redirects = $routes;
  }

}
