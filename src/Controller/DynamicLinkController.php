<?php

declare(strict_types=1);

namespace Drupal\dynamic_links\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\dynamic_links\DynamicLinkInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Returns responses for dynamic links.
 */
class DynamicLinkController implements ContainerInjectionInterface {

  use AutowireTrait;

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $httpKernel
   *   The HTTP kernel.
   */
  public function __construct(
    protected readonly AccountInterface $currentUser,
    protected readonly HttpKernelInterface $httpKernel,
  ) {}

  /**
   * Builds the response for the dynamic link.
   *
   * @param \Drupal\dynamic_links\DynamicLinkInterface $dynamic_link
   *   The dynamic link.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function redirect(DynamicLinkInterface $dynamic_link, Request $request): Response {
    $url = $dynamic_link->getFirstAvailable($this->currentUser)['url'];
    $generated_url = $url->toString(TRUE);
    $string_url = $generated_url->getGeneratedUrl();
    if (!$dynamic_link->useSubrequest()) {
      return new RedirectResponse($string_url);
    }
    // To correctly handle form submissions on a dynamic link page,
    // we must pass parameters from the original request in requests
    // other than GET.
    $params = $request->isMethod(Request::METHOD_GET)
      ? array_merge($request->query->all(), $url->getRouteParameters(), $url->getOption('query') ?? [])
      : $request->request->all();
    $subrequest = Request::create($string_url, $request->getMethod(), $params, $request->cookies->all(), $request->files->all(), $request->server->all(), $request->getContent(TRUE));
    if ($request->hasSession()) {
      $subrequest->setSession($request->getSession());
    }
    $response = $this->httpKernel->handle($subrequest, HttpKernelInterface::SUB_REQUEST);
    if ($response instanceof CacheableResponseInterface) {
      $response->addCacheableDependency($generated_url);
    }
    return $response;
  }

  /**
   * Checks access to the dynamic link.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check access for.
   * @param \Drupal\dynamic_links\DynamicLinkInterface $dynamic_link
   *   The target dynamic link.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, DynamicLinkInterface $dynamic_link): AccessResultInterface {
    $available = $dynamic_link->getFirstAvailable($account);
    return ($available['url'] ? AccessResult::allowed() : AccessResult::forbidden())->addCacheableDependency($available['cacheability']);
  }

}
