<?php

declare(strict_types=1);

namespace Drupal\dynamic_links;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of dynamic links.
 */
class DynamicLinkListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['status'] = $this->t('Status');
    $header['id'] = $this->t('ID');
    $header['url'] = $this->t('URL');
    $header['as_routes'] = $this->t('As routes');
    $header['use_subrequest'] = $this->t('Use subrequest');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\dynamic_links\DynamicLinkInterface $entity */
    $row['status'] = $entity->status() ? $this->t('On') : $this->t('Off');
    $row['id'] = $entity->id();
    $row['url'] = $entity->getPath();
    $row['as_routes'] = $entity->getRoutes() ? $this->t('Yes') : $this->t('No');
    $row['use_subrequest'] = $entity->useSubrequest() ? $this->t('Yes') : $this->t('No');
    return $row + parent::buildRow($entity);
  }

}
